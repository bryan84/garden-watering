//Solar panel powers up the microcontroller when the sun rises. There is no battery.
//Program waits several hours for the sun to be directly overhead, then opens up to 3 solenoid valves.
//If the sun isn't overhead-ish, the panel can't provide enough energy to activate the valves fully. 

int led0 = 0;  //green status LED
int switch1 = 1;  //module 1 with relay
int switch2 = 2;  //module 2 WITHOUT RELAY
int switch3 = 3;  //module 3 WITHOUT RELAY
int j = 0; //counting variable

void setup() {                
  pinMode(led0, OUTPUT);     
  pinMode(switch1, OUTPUT);     
  pinMode(switch2, OUTPUT);     
  pinMode(switch3, OUTPUT);     
  digitalWrite(led0, HIGH);  //turn on green status LED
  delay(5000);
}

void loop() {
  //turn on the first switch for five seconds
  /*digitalWrite(switch1, HIGH);
  while (j < 5) { 
    delay(1000);
    j++;
  }
  j = 0; */
  digitalWrite(switch1, LOW); 
  //turn on the second switch
  digitalWrite(switch2, HIGH);
  while (j < 5) { 
    delay(1000);
    j++;
  }
  j = 0;
  digitalWrite(switch2, LOW);
  //turn on the third switch
  digitalWrite(switch3, HIGH);
  while (j < 5) { 
    delay(1000);
    j++;
  }
  j = 0;
  digitalWrite(switch3, LOW);
}